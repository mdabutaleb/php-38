<?php

trait SoftDelete
{
    public function delete()
    {
        echo "Deleting from application but not from Database";
        parent::delete();
    }
}

class Profiles
{
    public function delete()
    {
        echo "<Br/>Deleting from Database";
    }
}

class Subclass extends Profiles
{
    use SoftDelete;
}