<?php
include_once("../../../../vendor/autoload.php");
use App\Bitm\SEIP\Students\Students;
$obj = new Students();
$value = $obj->setData($_GET)->show();
//echo "<Pre>";
//print_r($value);

?>
<html>
<head>
    <title>Student Information</title>
</head>
<body>
<fieldset>
    <legend>Edit student information</legend>
    <a href="index.php">See List</a>
    <form action="update.php" method="post">
        <label for="name">Student Name</label>
        <input type="text" id="name" name="title" value="<?php echo $value['title'] ?>"/>
        <input type="hidden" name="id" value="<?php echo $value['id']; ?>"/>
        <input type="submit" value="Update"/>
    </form>
</fieldset>
</body>

</html>
