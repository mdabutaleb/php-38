<?php
namespace App\Bitm\SEIP\Students;

use PDO;

class Students
{
    public $name = '';
    public $id = '';
    public $pdo = '';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->name = $data['title'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function index()
    {
        try {
            $query = "SELECT * FROM `students` WHERE deleted_at='0000-00-00 00:00:00'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function store()
    {
        try {
            $query = "INSERT INTO `students` (`id`, `unique_id`, `title`) VALUES (:a, :uid, :b)";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':a' => null,
                    ':uid' => uniqid(),
                    ':b' => $this->name
                )
            );
            if ($stmt) {
                $_SESSION['message'] = "Successfully submitted";
                header('location:create.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function show()
    {
        try {
            $query = "SELECT * FROM `students` WHERE unique_id='$this->id'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (empty($data)) {
                $_SESSION['message'] = "Opps something going wrong";
                header('location:index.php');
            } else {
                return $data;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $query = "UPDATE students SET title=:title where id=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':title' => $this->name
                )
            );
            if ($stmt) {
                $_SESSION['message'] = "Successfully Updated";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trash()
    {
        try {
            $query = "UPDATE students SET deleted_at=:datetme where id=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':datetme' => date('Y-m-d h:m:s'),
                )
            );
            if ($stmt) {
                $_SESSION['message'] = "Successfully Deleted";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trashlist()
    {

        try {
            $query = "SELECT * FROM `students` WHERE deleted_at!='0000-00-00 00:00:00'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function restore()
    {
        try {
            $query = "UPDATE students SET deleted_at=:datetme where id=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':datetme' => '0000-00-00 00:00:00',
                )
            );
            if ($stmt) {
                $_SESSION['message'] = "Successfully Restored !!!";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete()
    {
        try {
            $query = "DELETE FROM `students` WHERE `students`.`id` =$this->id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "<h2>Successfully Deleted</h2>";
                header('location:trashlist.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}

