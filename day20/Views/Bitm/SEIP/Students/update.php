<?php
include_once("../../../../vendor/autoload.php");
use App\Bitm\SEIP\Students\Students;

session_start();
$obj = new Students();

if (!empty($_POST['title'])) {
    if (preg_match("/([a-z*0-9_A-Z])/", $_POST['title'])) {
        $_POST['title'] = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
        $obj->setData($_POST)->update();
    } else {
        $_SESSION['message'] = "Invalid Input";
        header('location:create.php');
    }

} else {
    $_SESSION['message'] = "Input can't be empty";
    header('location:create.php');
}
