<?php
include_once("../../../../vendor/autoload.php");
use App\Bitm\SEIP\Students\Students;

$obj = new Students();
$allData = $obj->trashlist();
if (isset($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<html>
<head>
    <title>List of Students</title>
</head>
<body>
<a href="create.php">Add New</a> |
<a href="trashlist.php">See Deleted Items</a> |
<a href="index.php">See Original List</a>
<table border="1">
    <tr>
        <td>Serial</td>
        <td>Title</td>
        <td>Action</td>
    </tr>
    <?php
    $serial = 1;
    foreach ($allData as $key => $value) {
        ?>
        <tr>
            <td><?php echo $serial++ ?></td>
            <td><?php echo $value['title'] ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['unique_id'] ?>">View Details</a> |
                <a href="edit.php?id=<?php echo $value['id'] ?>"> Edit </a> |
                <a href="restore.php?id=<?php echo $value['id'] ?>"> Restore </a> |
                <a href="delete.php?id=<?php echo $value['id'] ?>">Delete</a>
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>