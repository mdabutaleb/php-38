<?php
include_once("../../../../vendor/autoload.php");
use App\Bitm\SEIP\Students\Students;
$obj = new Students();
$value = $obj->setData($_GET)->show();
?>
<html>
<head> <title>List of Students</title></head>
<body>
<a href="index.php">Back to list</a>
<table border="1">
    <tr>
        <td>Serial</td>
        <td>Title</td>
        <td>Action</td>
    </tr>
        <tr>
            <td><?php echo $value['id'] ?></td>
            <td><?php echo $value['title'] ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['id']  ?>"> Edit | Delete</a>
            </td>
        </tr>
</table>
</body>
</html>
