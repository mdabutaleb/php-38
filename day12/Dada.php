<?php

class Dada
{
    public function getBloodGroup()
    {
        echo "AB+";
    }

    public function get()
    {
        $this->getBloodGroup();
    }
}

$obj = new Dada();
$obj->getBloodGroup();


class Baba extends Dada
{
    public function done()
    {
        $this->getBloodGroup();
    }

}


class Dadar_Bondhu
{
    public function bondhur_method()
    {
        $dadar_object = new Dada();
        $dadar_object->getBloodGroup();
    }
}

$dadar_bondhur_object = new Dadar_Bondhu();
$dadar_bondhur_object->bondhur_method();


$babar_object = new Baba();
$babar_object->getBloodGroup();
