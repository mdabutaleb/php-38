<?php
namespace App\Bitm\SEIP\Students;
use PDO;
class Students
{
    public $name = '';

    public function setData($data = '')
    {
        $this->name = $data['title'];
        return $this;
    }

    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');
            $query = "INSERT INTO `students` (`id`, `title`) VALUES (:a, :b)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                    ':a' => null,
                    ':b' => $this->name
                )
            );
            if($stmt){
                session_start();
                $_SESSION['message'] = "Successfully submitted";
                header('location:create.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}

